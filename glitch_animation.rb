#!/usr/bin/env ruby
require_relative 'classes/glitcher.rb'
require_relative 'classes/animater.rb'
require 'optparse'

FILTERS = %w(none sub up average paeth random random_consistent)
ALGORITHMS = %w(exchange transpose)
global_options = {}
glitcher_options = {}
animater_options = {}

OptionParser.new do |opts|
  opts.banner = 'Usage: glitch_animation.rb [options]'

  opts.on('-i I', '--input I', String, 'Input png file (default: image.png)') do |i|
    global_options[:input] = i
  end

  opts.on('-o O', '--output O', String, 'Output gif file (default: output.gif)') do |o|
    global_options[:output] = o
  end

  opts.on('-v', '--verbose', 'Run verbosely') do 
    glitcher_options[:verbose] = animater_options[:verbose] = true
  end

  opts.on('--random', 'Add a random factor to the generated glitch (works with exchange algorithm)') do 
    glitcher_options[:random] = true
  end

  opts.on('--glitch-name N', String, 'Name of the glitched image created (default: glitch)') do |n|
    glitcher_options[:glitch_name] = n
  end

  opts.on('--glitch-folder F', String, 'Name of the folder where the glitched images are stored (default: glitched)') do |f|
    glitcher_options[:glitch_folder] = f
  end

  opts.on('--full-glitch', 'create a gif with only glitched images') do 
    animater_options[:full_glitched] = true
  end

  opts.on('--frames F', Integer, 'F glitched frames created (default: 1)') do |f|
    glitcher_options[:frames] = f
  end

  opts.on('--delay D', Integer, 'D ticks between each frames (1s is 100 ticks, default: 2)') do |d|
    animater_options[:delay] = d
  end

  opts.on('--first_delay D', Integer, 'D ticks after the first frame (default: 300)') do |d|
    animater_options[:first_delay] = d
  end

  opts.on('--filter FILTER', FILTERS, 'Select glitching filter :', "  (#{FILTERS.join(' ,')}) (default: sub)") do |filter|
    glitcher_options[:filter] = FILTERS.index(filter)
  end

  opts.on('--algorithm ALGORITHM', ALGORITHMS, 'Select glitching algorithm :', "  (#{ALGORITHMS.join(' ,')}) (default: exchange)") do |algorithm|
    glitcher_options[:algorithm] = algorithm
  end

  opts.on('--range R', Integer, 'Range of the glitch area (0 for full range, default: 0)') do |r|
    glitcher_options[:range] = r
  end

  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts
    exit
  end
end.parse!

input_image = global_options[:input] || 'image.png'
output_image = global_options[:output] || 'output.gif'
file_names = Glitcher.new(input_image, glitcher_options).call
images_to_animate = animater_options[:full_glitched] ? file_names : file_names.unshift(input_image)
Animater.new(images_to_animate, output_image, animater_options).call
puts "=> #{output_image}"
