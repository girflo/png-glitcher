#!/usr/bin/env ruby
require_relative 'classes/glitcher.rb'
require 'optparse'
require 'rmagick'

FILTERS = %w(none sub up average paeth random)
ALGORITHMS = %w(exchange transpose)
global_options = {}
glitcher_options = { frames: 1 }

OptionParser.new do |opts|
  opts.banner = "Usage: glitch_directory.rb [options]"

  opts.on("-i I", "--input-folder I", String, "Input folder storing png imges to glitch (default: input)") do |i|
    global_options[:input] = i
  end

  opts.on("-v", "--verbose", "Run verbosely") do 
    glitcher_options[:verbose] = true
  end

  opts.on("--random", "Add a random factor to the generated glitch (works with exchange algorithm)") do 
    glitcher_options[:random] = true
  end

  opts.on("--glitch-folder F", String, "Name of the folder where the glitched images are stored (default: glitched)") do |f|
    glitcher_options[:glitch_folder] = f
  end

  opts.on("--frames F", Integer, "F glitched frames created (default: 1)") do |f|
    glitcher_options[:frames] = f
  end

  opts.on("--filter FILTER", FILTERS, "Select glitching filter :", "  (#{FILTERS.join(',')}) (default: sub)") do |filter|
    glitcher_options[:filter] = FILTERS.index(filter)
  end

  opts.on("--algorithm ALGORITHM", ALGORITHMS, "Select glitching algorithm :", "  (#{ALGORITHMS.join(' ,')}) (default: exchange)") do |algorithm|
    glitcher_options[:algorithm] = algorithm
  end

  opts.on("--range R", Integer, "Range of the glitch area (0 for full range, default: 0)") do |r|
    glitcher_options[:range] = r
  end

  opts.on_tail("-h", "--help", "Show this message") do
    puts opts
    exit
  end
end.parse!

input_folder = global_options[:input] || 'input'
if File.directory?(input_folder)
  Dir.foreach(input_folder) do |item|
    next if ['.', '..', '.DS_Store'].include? item

    image_name = item.end_with?('.png') ? item.chomp('.png') : item
    glitcher_options[:glitch_name] = image_name
    Glitcher.new("#{input_folder}/#{item}", glitcher_options).call
  end
else
  puts "The given directory (#{input_folder}) doesn't exist"
end
