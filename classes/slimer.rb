require 'chunky_png'
class Slimer
  def initialize(input, input_type, colors, options = {})
    @input = input
    @input_type = input_type
    @colors = colors
    @direction = options[:direction] || :up_to_down
    @glitch_name = options[:glitch_name] || 'glitch'
    @glitch_folder = options[:glitch_folder] || 'glitched'
    @verbose = options[:verbose] || false
    @probability = options[:probability] || 95
    Dir.mkdir @glitch_folder unless File.directory?(@glitch_folder)
  end

  def call
    @input_type == "image" ? single_image_slim(@input, "#{@glitch_name}.png") : directory_slim
  end

  private

  def single_image_slim(input_image, output_filename)
    puts "Start the creation of the glitched images #{input_image}." if @verbose
    image = ChunkyPNG::Image.from_file(input_image)
    slimed_image = if @direction == :up_to_down
                     up_to_down_slim(image)
                   elsif @direction == :down_to_up
                     down_to_up_slim(image)
                   else
                     horizontal_slim(image)
                   end
    slimed_image.save(output_filename.to_s, interlace: true)
  end

  def directory_slim
    Dir.foreach(@input) do |item|
      next unless item.end_with?(".png")
      output_filename = "#{@glitch_name}_#{item.chomp('.png')}"
      single_image_slim("#{@input}/#{item}")
    end
  end

  def up_to_down_slim(image)
    (0..image.width - 1).each do |w|
      (1..image.height - 1).each do |h|
        next if rand(100) > @probability
        next unless color_to_glitch?(image[w,h-1])

        image[w,h] = image[w,h-1]
      end
    end
    image
  end

  def down_to_up_slim(image)
    (0..image.width - 1).each do |w|
      (0..image.height - 2).each do |h|
        next if rand(100) > @probability

        inverted_w = image.width - 1 - w
        inverted_h = image.height - 2 - h
        next unless color_to_glitch?(image[inverted_w,inverted_h+1])
        image[inverted_w,inverted_h] = image[inverted_w,inverted_h+1]
      end
    end
    image
  end

  def horizontal_slim(image)
    (1..image.width - 1).each do |w|
      (0..image.height - 1).each do |h|
        next if rand(100) > @probability
        next unless color_to_glitch?(image[w-1,h])

        image[w,h] = image[w-1,h]
      end
    end
    image
  end

  def color_to_glitch?(pixel)
    return true if @colors.size == 9

    hsl = pixel_hsl(pixel)
    pixel_color = pixel_color_group(hsl)
    @colors.include? pixel_color.to_sym
  end

  def pixel_hsl(pixel)
    ChunkyPNG::Color.to_hsl(pixel)
  end

  def pixel_color_group(hsl)
    return :light if hsl[2] > 0.8
    return :dark if hsl[2] < 0.2
    return :grey if hsl[1] < 0.25 && hsl[1] > 0.2
    return :red if hsl[0] < 30
    return :yellow if hsl[0] >= 30 && hsl[0] < 90
    return :green if hsl[0] >= 90 && hsl[0] < 150
    return :cyan if hsl[0] >= 150 && hsl[0] < 210
    return :blue if hsl[0] >= 210 && hsl[0] < 270
    return :magenta if hsl[0] >= 270 && hsl[0] < 350

    :red
  end
end

options = { direction: :down_to_up }
colors = %i(ligth green yellow)
Slimer.new('fleur.png', "image", colors, options).call
