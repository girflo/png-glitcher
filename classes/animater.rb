require 'RMagick'
class Animater
  def initialize(input_file_names, output_file_name, options = {})
    @input_file_names = input_file_names
    @output_file_name = output_file_name
    @delay = options[:delay] || 2
    @first_delay = options[:first_delay] || 300
    @verbose = options[:verbose] || false
    @full_glitched = options[:full_glitched] || false
  end

  def call
    puts "Start animation creation" if @verbose
    animation = Magick::ImageList.new(*@input_file_names)
    animation.delay = @delay
    animation.first.delay = @first_delay unless @full_glitched
    animation.write(@output_file_name)
    puts "Animation created" if @verbose
  end
end
