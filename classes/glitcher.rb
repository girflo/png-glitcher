require 'pnglitch'
class Glitcher
  def initialize(input_image, options = {})
    @input_image = input_image
    @filter = if options[:filter].nil?
                1
              elsif options[:filter] == 6
                #random_consistent filter
                rand(5)
              else
                options[:filter]
              end
    @frames = options[:frames] || 1
    @glitch_name = options[:glitch_name] || 'glitch'
    @glitch_folder = options[:glitch_folder] || 'glitched'
    @verbose = options[:verbose] || false
    @random = options[:random] || false
    @range = options[:range] || 0
    @algorithm = options[:algorithm] || 'exchange'
    Dir.mkdir @glitch_folder unless File.directory?(@glitch_folder)
  end

  def call
    puts "Start the creation of the glitched images #{@input_image}." if @verbose
    file_names = []
    if @frames == 1
      file_names << glitch(0, false)
    else
      @frames.times { |index| file_names << glitch(index) }
    end
    puts 'Glitched images created.' if @verbose
    file_names
  end

  private

  def glitch(index, show_index = true)
    glitch_filter = @filter == 5 ? rand(5) : @filter
    name = if show_index
             "#{@glitch_folder}/#{@glitch_name}#{index}.png"
           else
             "#{@glitch_folder}/#{@glitch_name}.png"
           end
    PNGlitch.open(@input_image) do |png|
      filtered_glitch(png, glitch_filter).save name
    end
    puts "Image #{index + 1} created => #{name}" if @verbose
    name
  end

  def give_me_a_letter(index = rand(26))
    ('a'..'z').to_a[index]
  end

  def filtered_glitch(png, custom_filter)
    png.each_scanline do |scanline|
      scanline.change_filter custom_filter
    end
    png.glitch do |data|
      if @algorithm == 'exchange'
        exchange_data(data)
      elsif @algorithm == 'transpose'
        transpose_data(data)
      # elsif @algorithm == 'defect'
      #   defect_data(data)
      end
    end
  end

  def exchange_data(data)
    if @range.zero?
      letter = @random ? give_me_a_letter.to_s : 'x'
      data.gsub /\d/, letter
    else
      @range.times do
        data[rand(data.size)] = give_me_a_letter.to_s
      end
      data
    end
  end

  # def defect_data(data)
  #   if @range.zero?
  #     data.gsub /\d/, ''
  #   else
  #     (@range / 5).times do
  #       data[rand(data.size)] = ''
  #     end
  #     data
  #   end
  # end

  def transpose_data(data)
    x = data.size / 4
    data[0, x] + data[x * 2, x] + data[x * 1, x] + data[x * 3..-1]
  end
end
