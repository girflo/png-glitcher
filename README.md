# PNG Glitcher
## Goal
This project is composed by three scripts created with [pnglitch](https://github.com/ucnv/pnglitch), each of them can be used to create glitched version of a png image : 

- glitch_sample.rb : create a folder `glitched` containing an image for each glitch algorithm.
- glitch_directory.rb : create an glitched version of every images contained by a directory.
- glitch_animation.rb : create an animate (.gif) from a .png image

## Requirements
In order to use this project you need the following software installed on your computer : 
- [ffmpeg](https://ffmpeg.org/)
- [ruby](https://www.ruby-lang.org/en/)

## Usage
### glitch_sample.rb
```
Usage: glitch_sample.rb [options]
    -i, --input I                    Input png file (default: image.png)
    -v, --verbose                    Run verbosely
    -h, --help                       Show this message
```

### glitch_directory.rb
```
Usage: glitch_directory.rb [options]
    -i, --input-folder I             Input folder storing png imges to glitch (default: input)
    -v, --verbose                    Run verbosely
        --random                     Add a random factor to the generated glitch (works with exchange algorithm)
        --glitch-folder F            Name of the folder where the glitched images are stored (default: glitched)
        --frames F                   F glitched frames created (default: 1)
        --filter FILTER              Select glitching filter :
                                       (none,sub,up,average,paeth,random) (default: sub)
        --algorithm ALGORITHM        Select glitching algorithm :
                                       (exchange ,transpose) (default: exchange)
        --range R                    Range of the glitch area (0 for full range, default: 0)
    -h, --help                       Show this message
```

### glitch_animation.rb
```
Usage: glitch_animation.rb [options]
    -i, --input I                    Input png file (default: image.png)
    -o, --output O                   Output gif file (default: output.gif)
    -v, --verbose                    Run verbosely
        --random                     Add a random factor to the generated glitch (works with exchange algorithm)
        --glitch-name N              Name of the glitched image created (default: glitch)
        --glitch-folder F            Name of the folder where the glitched images are stored (default: glitched)
        --full-glitch                create a gif with only glitched images
        --frames F                   F glitched frames created (default: 1)
        --delay D                    D ticks between each frames (1s is 100 ticks, default: 2)
        --first_delay D              D ticks after the first frame (default: 300)
        --filter FILTER              Select glitching filter :
                                       (none ,sub ,up ,average ,paeth ,random ,random_consistent) (default: sub)
        --algorithm ALGORITHM        Select glitching algorithm :
                                       (exchange ,transpose) (default: exchange)
        --range R                    Range of the glitch area (0 for full range, default: 0)
    -h, --help                       Show this message
```

## Examples
An animation and several glitched images can be found in the `images` directory. 
