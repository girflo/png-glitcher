#!/usr/bin/env ruby
require_relative 'classes/glitcher.rb'
require 'optparse'

FILTERS = %w(none sub up average paeth)
ALGORITHMS = %w(exchange transpose)
global_options = {}
glitcher_options = {}
OptionParser.new do |opts|
  opts.banner = 'Usage: glitch_sample.rb [options]'

  opts.on('-i I', '--input I', String, 'Input png file (default: image.png)') do |i|
    global_options[:input] = i
  end

  opts.on('-v', '--verbose', 'Run verbosely') do
    glitcher_options[:verbose] = true
  end

  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts
    exit
  end
end.parse!

[0, 50].each do |range|
  glitcher_options[:range] = range
  ALGORITHMS.each do |algorithm|
    glitcher_options[:algorithm] = algorithm
    FILTERS.each do |filter|
      glitcher_options[:glitch_name] = "glitch_range#{range}_#{algorithm}_#{filter}"
      glitcher_options[:filter] = filter
      glitcher_options[:frames] = 1
      input_image = global_options[:input] || 'image.png'
      Glitcher.new(input_image, glitcher_options).call
    end
  end
end
